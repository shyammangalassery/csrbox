# ================================================================
# standard Bluespec setup, defining environment variables 
# BLUESPECDIR, BLUESPEC_HOME and BLUESPEC_LICENSE_FILE, 
# and placing $BLUESPEC_HOME/bin in your path is expected to 
# invoke 'bsc', the Bluespec compiler.
# ================================================================
 
# Directory containing the box
TOPFILE:=csrbox.bsv
TOPMODULE:=mk_csrbox
TOPDIR:=./csrbox_work/bsv
BSC_DIR := $(shell which bsc)
BSC_VDIR:=$(subst /bsc,/,${BSC_DIR})../lib/Verilog/
XLEN=32
# Verilog simulator

# ================================================================
override define_macros += -D simulate -D pmp_grain=2 -D mhpm_eventcount=29 -D causesize=7 -D xlen=$(XLEN) -D csrbox_noinline

VERILOGDIR := ./verilog
BSVOUTDIR := ./bin
BUILDDIR := bsv_build
BSC_COMP_FLAGS = -u -verilog -elab -vdir $(VERILOGDIR) -bdir $(BUILDDIR) -info-dir $(BUILDDIR) \
								 +RTS -K40000M -RTS -check-assert  -keep-fires -opt-undetermined-vals \
								 -remove-false-rules -remove-empty-rules -remove-starved-rules -remove-dollar \
								 -unspecified-to X -show-schedule -show-module-use 

BSC_PATHS = -p ./csrbox_work/bsv:./common_bsv:%/Libraries:


default: clone_common generate_csrbox generate_verilog

.PHONY: clone_common
clone_common:
ifeq (,$(wildcard ./common_bsv/ConcatReg.bsv))
	git clone https://gitlab.com/shaktiproject/common_bsv.git
endif

.PHONY: generate_csrbox
generate_csrbox:
	csrbox -optro0 -ispec=examples/rv$(XLEN)i_isa.yaml -gspec=examples/csrgrp$(XLEN)_test.yaml --verbose debug -cspec examples/rv$(XLEN)i_custom.yaml -dspec examples/rv$(XLEN)i_debug.yaml

.PHONY: generate_csrbox_checked
generate_csrbox_checked:
	csrbox -optro0 --checked -ispec=build/rv$(XLEN)i_isa_checked.yaml -gspec=examples/csrgrp$(XLEN)_test.yaml --verbose debug -cspec build/rv$(XLEN)i_custom_checked.yaml -dspec build/rv$(XLEN)i_debug_checked.yaml

# ----------------------------------------------------------------
# Verilog compile/link/sim
# ----------------------------------------------------------------
.PHONY: generate_verilog
generate_verilog:
	@echo Compiling for Verilog ...
	@mkdir -p $(BUILDDIR) $(VERILOGDIR)
	bsc $(BSC_COMP_FLAGS) $(define_macros) $(BSC_PATHS) -g $(TOPMODULE)  $(TOPDIR)/$(TOPFILE) || (echo "BSC COMPILE ERROR"; exit 1) 
	@echo Compiling for Verilog finished

# ----------------------------------------------------------------

.PHONY: clean
clean:
	rm -r -f  build_bsim  bsv_build csrbox_work  verilog obj_dir bin *.bsv
	rm -f  *$(TOPMODULE)*  *.vcd 

