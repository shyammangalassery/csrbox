.. See LICENSE.incore for details

=====
Usage
=====

CLI
===

Once you have CSRBOX installed, executing ``csrbox --help`` should print the following on the terminal. ::

  Usage: csrbox [OPTIONS]
  
  Options:
    --version                       Show the version and exit.
    -v, --verbose [info|error|debug]
                                    Set verbose level
    -ispec, --isaspec PATH          RISCV-CONFIG ISA File
    -gspec, --grpspec PATH          Grouping YAML File
    --workdir PATH                  Work directory path
    --help                          Show this message and exit.

Importing in other Projects
===========================
To use CSRBOX in a project first install it and use the following python syntax::

    import csrbox

Test
====

To test the csrbox a sample makefile is provided. Run the following command to test::

    make generate_csrbox generate_verilog XLEN=32

