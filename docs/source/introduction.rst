############
Introduction
############

**CSRBOX** is a YAML based framework which can be used to generate bluespec files for a flow of groups containing control and status registers , that adhere to the RISC-V Priveleged Specification. 

**Caution**: This is still a work in progress and non-backward compatible changes are expected to happen. 

The current version CSRBOX adheres to the latest version of RISC-V Priveleged Spec v1.12, check here: `Priv Spec latest <https://github.com/riscv/riscv-isa-manual/>`_

For more information on the official RISC-V spec please visit: `RISC-V Specs <https://riscv.org/specifications/>`_

CSRBOX [`Repository <https://gitlab.com/incoresemi/ex-box/csrbox/>`_]


